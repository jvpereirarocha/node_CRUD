const express = require('express')
const bodyParser = require('body-parser')
const app = express()
const port = 3005
const MongoClient = require('mongodb').MongoClient
const uri = "mongodb+srv://jv:sci@2017@cluster0-xlafe.mongodb.net/test?retryWrites=true&w=majority"

MongoClient.connect(uri, (err, client) => {
    if (err) {
        return console.log(err)
    }
    db = client.db('cluster0')
})

app.set('view engine', 'ejs')
app.use(bodyParser.urlencoded({ extended: true }))

app.get('/', (request, response) => {
    //response.send('Welcome to the CRUD with Node, Express e MongoDB')
    response.render('index.ejs')
})

app.get('/list', (request, response) => {
    let cursor = db.collection('data').find()
})

app.get('/show', (request, response) => {
    db.collection('data').find().toArray((err, results) => {
        if (err) {
            return console.log(err)
        }
        response.render('show.ejs', {data: results})
    })
})

app.post('/show', (request, response) => {
    db.collection('data').insertOne(request.body, (err, result) => {
        if (err) {
            return console.log(err)
        }
        console.log('Salvo no banco de dados')
        response.redirect('/show')
    })
})

app.listen(port, () => {
    console.log(`The server is running at the port ${port}.`)
})